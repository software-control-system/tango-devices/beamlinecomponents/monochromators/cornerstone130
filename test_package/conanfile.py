import os
from conan import ConanFile
from conan.tools.build import can_run


class CornerStone130TestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        if can_run(self):
            if self.settings.os == "Windows":
                self.run("ds_CornerStone130 2>&1 | findstr \"usage\"", env="conanrun")
            else:
                self.run("ds_CornerStone130 2>&1 | grep \"usage\"", env="conanrun")
